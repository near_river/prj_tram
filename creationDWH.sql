ALTER AUTHORIZATION ON DATABASE::tes_bi TO sa


CREATE TABLE [dbo].[offre](
	[id] [int] primary key ,
	[valeur] varchar(100)
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[prix](
	[id] [int] primary key ,
	[valeur] varchar(100)
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[type](
	[id] [int] primary key ,
	[valeur] varchar(100)
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[point_de_vente](
	[id] [int] primary key ,
	[valeur] varchar(100)
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[periode](
	[id] [int] primary key ,
	[valeur] varchar(100)
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[date](
	[id] [int] primary key ,
	[valeur] varchar(100)
) ON [PRIMARY]

GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Fait](
	[id] [int] NOT NULL,
	[prix] [int] ,
	[type] [int] ,
	[offre] [int] ,
	[point_de_vente] [int] ,
	[date] [int] ,
	[periode] [int] ,
	[nombre] [int] ,
	[chiffre_d_affaire] [float] ,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Fait]  WITH CHECK ADD  CONSTRAINT [FK_Fait_date] FOREIGN KEY([date])
REFERENCES [dbo].[date] ([id])
GO

ALTER TABLE [dbo].[Fait] CHECK CONSTRAINT [FK_Fait_date]
GO

ALTER TABLE [dbo].[Fait]  WITH CHECK ADD  CONSTRAINT [FK_Fait_offre] FOREIGN KEY([offre])
REFERENCES [dbo].[offre] ([id])
GO

ALTER TABLE [dbo].[Fait] CHECK CONSTRAINT [FK_Fait_offre]
GO

ALTER TABLE [dbo].[Fait]  WITH CHECK ADD  CONSTRAINT [FK_Fait_periode] FOREIGN KEY([periode])
REFERENCES [dbo].[periode] ([id])
GO

ALTER TABLE [dbo].[Fait] CHECK CONSTRAINT [FK_Fait_periode]
GO

ALTER TABLE [dbo].[Fait]  WITH CHECK ADD  CONSTRAINT [FK_Fait_point_de_vente] FOREIGN KEY([point_de_vente])
REFERENCES [dbo].[point_de_vente] ([id])
GO

ALTER TABLE [dbo].[Fait] CHECK CONSTRAINT [FK_Fait_point_de_vente]
GO

ALTER TABLE [dbo].[Fait]  WITH CHECK ADD  CONSTRAINT [FK_Fait_prix] FOREIGN KEY([prix])
REFERENCES [dbo].[prix] ([id])
GO

ALTER TABLE [dbo].[Fait] CHECK CONSTRAINT [FK_Fait_prix]
GO

ALTER TABLE [dbo].[Fait]  WITH CHECK ADD  CONSTRAINT [FK_Fait_type] FOREIGN KEY([type])
REFERENCES [dbo].[type] ([id])
GO

ALTER TABLE [dbo].[Fait] CHECK CONSTRAINT [FK_Fait_type]
GO


