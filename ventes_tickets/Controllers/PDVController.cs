﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ventes_tickets.Models;

namespace ventes_tickets.Controllers
{
    [Authorize]
    public class PDVController : Controller
    {
        private ventesContainer db = new ventesContainer();

        //
        // GET: /PDV/

        public ActionResult Index()
        {
            return View(db.Point_de_venteSet.ToList());
        }

        //
        // GET: /PDV/Details/5

        public ActionResult Details(int id = 0)
        {
            Point_de_vente point_de_vente = db.Point_de_venteSet.Find(id);
            if (point_de_vente == null)
            {
                return HttpNotFound();
            }
            return View(point_de_vente);
        }

        //
        // GET: /PDV/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PDV/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Point_de_vente point_de_vente)
        {
            if (ModelState.IsValid)
            {
                db.Point_de_venteSet.Add(point_de_vente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(point_de_vente);
        }

        //
        // GET: /PDV/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Point_de_vente point_de_vente = db.Point_de_venteSet.Find(id);
            if (point_de_vente == null)
            {
                return HttpNotFound();
            }
            return View(point_de_vente);
        }

        //
        // POST: /PDV/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Point_de_vente point_de_vente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(point_de_vente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(point_de_vente);
        }

        //
        // GET: /PDV/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Point_de_vente point_de_vente = db.Point_de_venteSet.Find(id);
            if (point_de_vente == null)
            {
                return HttpNotFound();
            }
            return View(point_de_vente);
        }

        //
        // POST: /PDV/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Point_de_vente point_de_vente = db.Point_de_venteSet.Find(id);
            db.Point_de_venteSet.Remove(point_de_vente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}