﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ventes_tickets.Models;

namespace ventes_tickets.Controllers
{
    [Authorize]
    public class TypeTicketController : Controller
    {
        private ventesContainer db = new ventesContainer();

        //
        // GET: /TypeTicket/

        public ActionResult Index()
        {
            return View(db.Type_ticketSet.ToList());
        }

        //
        // GET: /TypeTicket/Details/5

        public ActionResult Details(int id = 0)
        {
            Type_ticket type_ticket = db.Type_ticketSet.Find(id);
            if (type_ticket == null)
            {
                return HttpNotFound();
            }
            return View(type_ticket);
        }

        //
        // GET: /TypeTicket/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TypeTicket/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Type_ticket type_ticket)
        {
            if (ModelState.IsValid)
            {
                db.Type_ticketSet.Add(type_ticket);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(type_ticket);
        }

        //
        // GET: /TypeTicket/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Type_ticket type_ticket = db.Type_ticketSet.Find(id);
            if (type_ticket == null)
            {
                return HttpNotFound();
            }
            return View(type_ticket);
        }

        //
        // POST: /TypeTicket/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Type_ticket type_ticket)
        {
            if (ModelState.IsValid)
            {
                db.Entry(type_ticket).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(type_ticket);
        }

        //
        // GET: /TypeTicket/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Type_ticket type_ticket = db.Type_ticketSet.Find(id);
            if (type_ticket == null)
            {
                return HttpNotFound();
            }
            return View(type_ticket);
        }

        //
        // POST: /TypeTicket/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Type_ticket type_ticket = db.Type_ticketSet.Find(id);
            db.Type_ticketSet.Remove(type_ticket);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}