﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ventes_tickets.Models;

namespace ventes_tickets.Controllers
{
    public class TicketController : Controller
    {
        private ventesContainer db = new ventesContainer();

        //
        // GET: /Ticket/

        public ActionResult Index()
        {
            try
            {
                ViewData["role"] = db.UserProfile.SingleOrDefault(e => e.UserName == User.Identity.Name).role;
            }
            catch { }
            string role = db.UserProfile.SingleOrDefault(e => e.UserName == User.Identity.Name).role;
            if (String.Equals(role, "Admin"))
                return View(db.TicketsSet.ToList());
            else
                return View(db.TicketsSet.Where(e => e.UserProfile.UserName == User.Identity.Name).ToList());
        }

        //
        // GET: /Ticket/Details/5

        public ActionResult Details(int id = 0)
        {
            Tickets tickets = db.TicketsSet.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            return View(tickets);
        }

        //
        // GET: /Ticket/Create

        public ActionResult Create()
        {

            ViewData["type"] = db.Type_ticketSet.ToList();
            ViewData["PDV"] = db.Point_de_venteSet.ToList();
            ViewData["date"] = DateTime.Now.ToString("dd/MM/yyyy");
            
            return View();
        }

        //
        // POST: /Ticket/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tickets tickets)
        {
            if (ModelState.IsValid)
            {
                tickets.Point_de_venteSet = db.Point_de_venteSet.SingleOrDefault(e => e.Id == tickets.Point_de_vente_Id);
                tickets.Type_ticketSet = db.Type_ticketSet.SingleOrDefault(e => e.Id == tickets.Type_ticket_Id);
                tickets.UserProfile = db.UserProfile.SingleOrDefault(e => e.UserName == User.Identity.Name);
                tickets.UserProfile_UserId = db.UserProfile.SingleOrDefault(e => e.UserName == User.Identity.Name).UserId;
                db.TicketsSet.Add(tickets);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tickets);
        }


        //demande de suppression
        public ActionResult Demande_supp(int id = 0)
        {
            Tickets tickets = db.TicketsSet.Find(id);
            tickets.a_supp = 1;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        //
        // GET: /Ticket/Edit/5

        public ActionResult Edit(int id = 0)
        {

            ViewData["type"] = db.Type_ticketSet.ToList();
            ViewData["PDV"] = db.Point_de_venteSet.ToList();
            Tickets tickets = db.TicketsSet.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            return View(tickets);
        }

        //
        // POST: /Ticket/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Tickets tickets)
        {
            if (ModelState.IsValid)
            {
                tickets.Point_de_venteSet = db.Point_de_venteSet.SingleOrDefault(e => e.Id == tickets.Point_de_vente_Id);
                tickets.Type_ticketSet = db.Type_ticketSet.SingleOrDefault(e => e.Id == tickets.Type_ticket_Id);
                tickets.UserProfile = db.UserProfile.SingleOrDefault(e => e.UserName == User.Identity.Name);
                tickets.UserProfile_UserId = db.UserProfile.SingleOrDefault(e => e.UserName == User.Identity.Name).UserId;
                
                db.Entry(tickets).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tickets);
        }

        //
        // GET: /Ticket/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Tickets tickets = db.TicketsSet.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            return View(tickets);
        }

        //
        // POST: /Ticket/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tickets tickets = db.TicketsSet.Find(id);
            db.TicketsSet.Remove(tickets);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}