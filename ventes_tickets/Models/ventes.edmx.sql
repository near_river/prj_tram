
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/09/2016 22:15:43
-- Generated from EDMX file: d:\users\nzerai\documents\visual studio 2013\Projects\ventes_tickets\ventes_tickets\Models\ventes.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ventes_tickets];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Type_ticketTickets]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TicketsSet] DROP CONSTRAINT [FK_Type_ticketTickets];
GO
IF OBJECT_ID(N'[dbo].[FK_UserProfileTickets]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TicketsSet] DROP CONSTRAINT [FK_UserProfileTickets];
GO
IF OBJECT_ID(N'[dbo].[FK_Point_de_venteTickets]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TicketsSet] DROP CONSTRAINT [FK_Point_de_venteTickets];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[TicketsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TicketsSet];
GO
IF OBJECT_ID(N'[dbo].[Type_ticketSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Type_ticketSet];
GO
IF OBJECT_ID(N'[dbo].[UserProfile]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserProfile];
GO
IF OBJECT_ID(N'[dbo].[Point_de_venteSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Point_de_venteSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'TicketsSet'
CREATE TABLE [dbo].[TicketsSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [date_achat] datetime  NOT NULL,
    [qte] int  NOT NULL,
    [Type_ticket_Id] int  NOT NULL,
    [UserProfile_UserId] int  NOT NULL,
    [Point_de_vente_Id] int  NOT NULL
);
GO

-- Creating table 'Type_ticketSet'
CREATE TABLE [dbo].[Type_ticketSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nom_type] varchar(100)  NOT NULL,
    [prix_type] float  NOT NULL,
    [periode] varchar(100)  NULL,
    [offre] varchar(100)  NOT NULL
);
GO

-- Creating table 'UserProfile'
CREATE TABLE [dbo].[UserProfile] (
    [UserId] int IDENTITY(1,1) NOT NULL,
    [UserName] nvarchar(56)  NOT NULL,
    [nom_complet] nvarchar(max)  NOT NULL,
    [role] nvarchar(max)  NOT NULL,
    [email] nvarchar(max)  NOT NULL,
    [adresse] nvarchar(max)  NOT NULL
);
GO


-- Creating table 'Point_de_venteSet'
CREATE TABLE [dbo].[Point_de_venteSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nom] varchar(100)  NOT NULL,
    [adresse] varchar(100)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'TicketsSet'
ALTER TABLE [dbo].[TicketsSet]
ADD CONSTRAINT [PK_TicketsSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Type_ticketSet'
ALTER TABLE [dbo].[Type_ticketSet]
ADD CONSTRAINT [PK_Type_ticketSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [UserId] in table 'UserProfile'
ALTER TABLE [dbo].[UserProfile]
ADD CONSTRAINT [PK_UserProfile]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [Id] in table 'Point_de_venteSet'
ALTER TABLE [dbo].[Point_de_venteSet]
ADD CONSTRAINT [PK_Point_de_venteSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Type_ticket_Id] in table 'TicketsSet'
ALTER TABLE [dbo].[TicketsSet]
ADD CONSTRAINT [FK_Type_ticketTickets]
    FOREIGN KEY ([Type_ticket_Id])
    REFERENCES [dbo].[Type_ticketSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Type_ticketTickets'
CREATE INDEX [IX_FK_Type_ticketTickets]
ON [dbo].[TicketsSet]
    ([Type_ticket_Id]);
GO

-- Creating foreign key on [UserProfile_UserId] in table 'TicketsSet'
ALTER TABLE [dbo].[TicketsSet]
ADD CONSTRAINT [FK_UserProfileTickets]
    FOREIGN KEY ([UserProfile_UserId])
    REFERENCES [dbo].[UserProfile]
        ([UserId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserProfileTickets'
CREATE INDEX [IX_FK_UserProfileTickets]
ON [dbo].[TicketsSet]
    ([UserProfile_UserId]);
GO

-- Creating foreign key on [Point_de_vente_Id] in table 'TicketsSet'
ALTER TABLE [dbo].[TicketsSet]
ADD CONSTRAINT [FK_Point_de_venteTickets]
    FOREIGN KEY ([Point_de_vente_Id])
    REFERENCES [dbo].[Point_de_venteSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Point_de_venteTickets'
CREATE INDEX [IX_FK_Point_de_venteTickets]
ON [dbo].[TicketsSet]
    ([Point_de_vente_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------